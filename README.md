# Wingify Backend Task

## Click here to go to the [Deployed API](https://floating-retreat-65224.herokuapp.com/api)

### The server uses a time based cache that store the result of api call for a maximum of 10 minutes after which the corresponding entry from the cache is deleted. 

## Prerequisites
* Nodejs

## Instruction to start development
1. clone the repository
2. go the the project root folder
3. install packages by running `npm install` in the terminal.
4. start the server by running `npm run start`
5. head over to localhost:5000 to use api endpoint.

## API Endpoint
* /api?category=[category]&country=[country]&keyword=[keyword]        
        
        
        Query params:
        1. category - category of news 
        2. country - country of origin 
        3. keyword - keyword to search in title 

    #### Note: to search the news through api either category or country is required

## API Endpoint Example
* /https://floating-retreat-65224.herokuapp.com/api?category=politics&country=us&keyword=trump

## Running Test Suite
* Run command `npm run test` to run test suit.
