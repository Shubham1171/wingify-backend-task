const request = require('supertest');
const app = require('../app');

describe('GET /api', () => {
    it('should return error 400 when category or country is not provided', async () => {
        const error = await request(app).get('/api');
        expect(error.statusCode).toBe(400);
        expect(error.body.status).toBe('bad request');
        expect(error.body.message).toBe('You need to provide country or catergory param');
    });
    it('should return correct result for category=asdf', async () => {
        const response = await request(app).get('/api?category=asdf');
        expect(response.statusCode).toBe(200);
    });
    it('should return correct result for country=us', async () => {
        const response = await request(app).get('/api?category=us');
        expect(response.statusCode).toBe(200);
    });
});
