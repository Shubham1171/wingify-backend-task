const app = require('./app');

const port = process.env.PORT || 5000;

app.listen(port, error => {
    if (error) console.log(error);
    else console.log('Server running on port 5000');
});
