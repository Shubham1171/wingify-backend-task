const axios = require('axios');
const Cache = require('../services/cacheService');

const cache = new Cache(600);

const fetchNews = async (req, res) => {
    // Function to make api request
    const fetchFromApi = (country, category, keyword) => () =>
        axios
            .get('https://newsapi.org/v2/top-headlines', {
                params: {
                    country,
                    category,
                    apiKey: 'eadb6da4bb5847a8b5f5b8a633e53ab9'
                }
            })
            .then(({ data }) => {
                return data.articles;
            })
            .then(articles => {
                if (!keyword) return articles;

                return articles.filter(article => {
                    return article.title.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                });
            })
            .then(articles => {
                // Transforming response
                return articles.map(article => {
                    const { title, content, url } = article;
                    const descriptionWords = content && content.split(' ');
                    return {
                        country,
                        category,
                        filterKeyword: keyword,
                        newsTitle: title,
                        description:
                            descriptionWords &&
                            descriptionWords
                                .splice(0, Math.min(100, descriptionWords.length))
                                .join(' '),
                        sourceNewsUrl: url
                    };
                });
            });

    const { category, country, keyword } = req.query;

    // If required params are not specified
    if (!country && !category)
        res.status(400).send({
            status: 'bad request',
            message: 'You need to provide country or catergory param'
        });
    else {
        // Key for searching and storing in cache
        const cacheKey = JSON.stringify({
            category,
            country,
            keyword
        });

        // Searching cache
        cache
            .get(cacheKey, fetchFromApi(country, category, keyword))
            .then(articles => {
                res.send({
                    status: 'ok',
                    totalResults: articles.length,
                    articles
                });
            })
            .catch(error => {
                res.status(500).send({
                    status: 'error',
                    message: 'Internal server error'
                });
            });
    }
};

module.exports = {
    fetchNews
};
