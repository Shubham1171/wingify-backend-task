const NodeCache = require('node-cache');

class Cache {
    constructor(ttlSeconds) {
        this.cache = new NodeCache({
            stdTTL: ttlSeconds,
            checkperiod: ttlSeconds * 0.5,
            useClones: false
        });
    }

    get(key, storeFunction) {
        const value = this.cache.get(key);
        // Cache hit
        if (value) return Promise.resolve(value);

        // Cache miss
        return storeFunction().then(result => {
            this.cache.set(key, result);
            return Promise.resolve(result);
        });
    }

    flush() {
        this.cache.flushAll();
    }
}

module.exports = Cache;
