const express = require('express');
const controller = require('./controllers');

const app = express();

app.get('/api', controller.fetchNews);

module.exports = app;
